@extends('layouts.adm')
@section('content')
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assessment_turned_in</i>
        </div>
        <p class="card-category">Registros do Dia</p>
        <h3 class="card-title">{{$today->count()}}
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">refresh</i>
          <a href="{{ url('Adm/Email/Exportar/Hoje')}}">Visualizar todos os registros do dia</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment</i>
        </div>
        <p class="card-category">Total de Registros</p>
        <h3 class="card-title">{{$clients->count()}}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> <a href="{{ url('Adm/Email/Exportar')}}">Visualizar todos os registros</a>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-header card-header-tabs card-header-primary">
        <div class="nav-tabs-navigation">
          <div class="nav-tabs-wrapper">
            <span class="nav-tabs-title">E-mail's</span>
            <span class="nav-item pull-right">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Buscar..." style="width:280px;">
                <button type="button" class="btn btn-white btn-round btn-just-icon" disabled>
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>E-mail</th>
                <th style="width:15%;">Data de Registro</th>
              </tr>
            </thead>
            <tbody>
              @forelse($clients as $client)
              @if($loop->iteration < 15)
              <tr class="objects" nome="{{$client->nome}}">
              @else
              <tr class="objects" nome="{{$client->nome}}" style="display:none;">
              @endif
                <td>{{$client->email}}</td>
                <td>{{$client->created_at}}</td>
              </tr>
              @empty
              <tr>
                <td colspan="3" class="text-center">Nenhum registro encontrado!</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
