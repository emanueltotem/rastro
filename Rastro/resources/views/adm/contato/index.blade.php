@extends('layouts.adm')
@section('content')
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assessment_turned_in</i>
        </div>
        <p class="card-category">Registros do Dia</p>
        <h3 class="card-title">{{$today->count()}}
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">refresh</i>
          <a href="{{ url('Adm/Contato/Exportar/Hoje')}}">Visualizar todos os registros do dia</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment</i>
        </div>
        <p class="card-category">Total de Registros</p>
        <h3 class="card-title">{{$clients->count()}}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> <a href="{{ url('Adm/Contato/Exportar')}}">Visualizar todos os registros</a>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-header card-header-tabs card-header-primary">
        <div class="nav-tabs-navigation">
          <div class="nav-tabs-wrapper">
            <span class="nav-tabs-title">E-mail's</span>
            <span class="nav-item pull-right">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Buscar..." style="width:280px;">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th style="width:20%;">Nome</th>
                <th style="width:20%;">E-mail</th>
                <th style="width:25%;">Interesse</th>
                <th style="width:15%;">Data de Registro</th>
                <th class="text-center">Ações</th>
              </tr>
            </thead>
            <tbody>
              @forelse($clients as $client)
              @if($loop->iteration < 15)
              <tr class="objects" value="{{$client->nome}}">
              @else
              <tr class="objects" value="{{$client->nome}}" style="display:none;">
              @endif
                <td>{{$client->nome}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->interesse}}</td>
                <td>{{$client->created_at->format('d/m/Y h:m')}}</td>
                <td class="text-center">
                  <button type="button" rel="tooltip" title="Ver Informações" class="btn btn-primary btn-sm" onclick="showObject('{{$client->nome}}','{{$client->email}}','{{$client->fone}}','{{$client->interesse}}','{{$client->mensagem_contato}}')" data-toggle="modal" data-target="#showObject">
                    <i class="material-icons">description</i>
                  </button>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" class="text-center">Nenhum registro encontrado!</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="showObject">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Contato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <small>Nome</small>
            <h4 id="bNome"></h4>
          </div>
          <div class="col-md-6">
            <small>E-mail</small>
            <h4 id="bEmail"></h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <small>Telefone</small>
            <h4 id="bFone"></h4>
          </div>
          <div class="col-md-6">
            <small>Interesse</small>
            <h4 id="bInteresse"></h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <small>Mensagem</small>
            <h4 id="bDescricao"></h4>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
@endsection
