@extends('layouts.adm')
@section('content')
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assessment_turned_in</i>
        </div>
        <p class="card-category">Registros do Dia</p>
        <h3 class="card-title">{{$today->count()}}
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">refresh</i>
          <a href="{{ url('Adm/Ligar/Exportar/Hoje')}}">Visualizar todos os registros do dia</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment</i>
        </div>
        <p class="card-category">Total de Registros</p>
        <h3 class="card-title">{{$clients->count()}}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> <a href="{{ url('Adm/Ligar/Exportar/Hoje')}}" target="_blank">Visualizar todos os registros</a>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-header card-header-tabs card-header-primary">
        <div class="nav-tabs-navigation">
          <div class="nav-tabs-wrapper">
            <span class="nav-tabs-title">E-mail's</span>
            <span class="nav-item pull-right">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Buscar..." style="width:280px;" onkeyup="filter(this.value)">
                <button type="button" class="btn btn-white btn-round btn-just-icon" disabled>
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th style="width:25%;">Nome</th>
                <th style="width:20%;">Telefone</th>
                <th style="width:20%;">Interesse</th>
                <th style="width:15%;">Data de Registro</th>
                <th class="text-center">Ações</th>
              </tr>
            </thead>
            <tbody>
              @forelse($clients as $client)
              @if($loop->iteration < 15)
              <tr class="objects" nome="{{$client->nome}}">
              @else
              <tr class="objects" nome="{{$client->nome}}" style="display:none;">
              @endif
                <td>{{$client->nome}}</td>
                <td>{{$client->fone}}</td>
                <td>{{$client->interesse}}</td>
                <td>{{$client->created_at->format('d/m/Y H:m')}}</td>
                <td class="text-center" id="{{$client->id}}_element_column">
                  @if($client->status_ligacao == true)
                  Ligado!
                  @else
                  <button type="button" rel="tooltip" title="Confirmar a ligação" class="btn btn-primary btn-sm" onclick="confirmCall('{{$client->id}}')">
                    <i class="material-icons">done</i>
                  </button>
                  @endif
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" class="text-center">Nenhum registro encontrado!</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
