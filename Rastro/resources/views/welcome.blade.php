<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Rastro Pneus - Juazeiro do Norte</title>
    <link rel="icon" type="image/ico"  href="/img/logo.png')}}">
    <link href="{{ asset('asset/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('asset/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('asset/css/site.css')}}" rel="stylesheet">
    <link href="{{ asset('asset/css/slick.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('asset/css/floating-wpp.min.css')}}">


    <!-- Custom Fonts -->
    <link href="{{ asset('asset/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700,900" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" id="logo-navbar" href="index.php"><img src="{{ asset('asset/img/logo.png')}}"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="page-scroll active">
                        <a href="index.php">Home</a>
                    </li>
                    <li class="page-scroll">
                        <a href="empresa.php">Empresa</a>
                    </li>
                    <li class="page-scroll">
                        <a href="servicos.php">Serviços</a>
                    </li>
                    <li class="page-scroll">
                        <a href="contato.php">Contato</a>
                    </li>
                    <li class="fone">
                        <i class="fa fa-phone"></i> (88) 3587-4195
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- Footer -->
        <footer class="text-center">
            <div class="footer-above">
                <div class="container">

                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="{{ asset('img/logo-white.png')}}">
                            </div>
                            <div class="col-lg-7 text-left address">
                                <p class="mini">Av. Padre Cícero, 1693 <br>Bairro Salesianos</br>
                                                Juazeiro do norte
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                    </div>

                    <div class="col-lg-4">
                        <h5><i class="fa fa-phone"></i> (88) 3587-4195</h5>
                          <a href="https://www.facebook.com/rastropneusoficial/" target="_blank"><img src="img/facebook.png" alt=""></a>
                          <a href="https://www.instagram.com/rastropneus/" target="_blank"><img src="img/instagram.png" alt=""></a>
                    </div>
                </div>
            </div>
        </footer>

        <script src="{{ asset('asset/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{ asset('asset/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('asset/js/jquery.mask.js')}}"></script>
        <script type="text/javascript">
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="{{ asset('asset/js/freelancer.min.js')}}"></script>
        <script src="{{ asset('asset/vendor/slick/slick.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('.customer-logos').slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 1000,
                    arrows: false,
                    dots: false,
                        pauseOnHover: false,
                        responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    }, {
                        breakpoint: 520,
                        settings: {
                            slidesToShow: 2
                        }
                    }]
                });
            });
        </script>
        <script src="http://malsup.github.com/jquery.form.js"></script>
        <script>
          // FORMULÁRIO DE E-MAIL
          $('#contact-form').submit(function(){
            $(this).ajaxSubmit({
              success: function($r) {
                console.log($r);
                if($r == "sucesso"){
                  alert("SUA SOLICITAÇÃO FOI ENVIADA!");

                }else{
                  alert("ERROR: " + $r);

                }
              }
            });
            return false;
          });

            function showDescription(id){

              switch (id) {
                case 1:
                  $("#titleModal").html("DUNLOP");
                  $("#contentModal").html("Há mais de 120 anos, ou especificamente em 1888, nasce a Dunlop. Nesse ano também nasce o primeiro pneu movido a câmara de ar, o pneu “pneumático”. Coincidência? Nenhuma.");
                  $("#contentModalButton").html("<a href='http://dunloppneus.com.br/encontre-seu-pneu-veiculo?p_ca=-2&p_ma=-2&p_mo=-2&p_se=-2&p_an=-2' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                case 2:
                  $("#titleModal").html("YOKOHAMA");
                  $("#contentModal").html("A Yokohama Rubber Co. Ltda, é fabricante líder na indústria mundial de pneus. Além disso, a empresa aplica seus conhecimentos em borracha à diversas e bem sucedidas linhas de negócios.");
                  $("#contentModalButton").html("<a href='https://www.yokohamatruck.com/tires/by-application/all/all/all' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                case 3:
                  $("#titleModal").html("OVATION TIRES");
                  $("#contentModal").html("A Shandong Hengfeng Rubber & Plastic Co., Ltd., um dos principais fabricantes de pneus na China, foi criada em 1995 e abrange uma área de cerca de 1,6 quilômetros quadrados, com ativos totais de mais de 5 bilhões de RMB.");
                  $("#contentModalButton").html("<a href='http://www.ovationtires.com/products_list/FrontColumns_navigation01-1446110682935FirstColumnId=42.html' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                case 4:
                  $("#titleModal").html("PIRELLI");
                  $("#contentModal").html("Mais de 100 anos de história da marca. Em 1908, a Pirelli adotou o P alongado como logotipo, que passou a identificar a empresa ao redor do mundo, consolidando assim a identidade da marca.");
                  $("#contentModalButton").html("<a href='https://corporate.pirelli.com/corporate/en-ww/aboutus/aboutus?_ga=2.66612526.1469885481.1529590807-1623239128.1529343374' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                case 5:
                  $("#titleModal").html("WEST LAKE");
                  $("#contentModal").html("Desde 1958, a Westlake produz uma linha completa de pneus de alta qualidade. Número um na produção de pneus na China e entre as dez maiores fabricantes do mundo, a marca conta com mais de 10.000 distribuidores pelo mundo. A Westlake conta com várias outras marcas próprias, como: Chaoyang, Agrimaster, Goodride, entre outras.");
                  $("#contentModalButton").html("<a href='http://www.westlakepneus.com.br/pt/index.php/about' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                case 6:
                  $("#titleModal").html("GT RADIAL");
                  $("#contentModal").html("A GT Radial possui um sistema de controle de qualidade em todas suas fábricas de pneus. Preocupada em sempre garantir que a qualidade das matérias-primas atendam às especificações exigidas.");
                  $("#contentModalButton").html("<a href='http://www.gtradialpneus.com.br/About-Us' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                case 7:
                  $("#titleModal").html("FATEO");
                  $("#contentModal").html("Constituído com capitais argentinos, o FATE inicia suas atividades em uma pequena unidade de mil m2 no bairro de Saavedra, cidade de Buenos Aires. Produz tecidos impermeáveis, trilhos para reparação de pneus e outros produtos de borracha.");
                  $("#contentModalButton").html("<a href='https://www.fate.com.ar/assets/admin/archivos/neumaticolineas/pdf/fate_auto1.pdf' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                  break;
                 case 8:
                    $("#titleModal").html("RYDANZ");
                    $("#contentModal").html("A Horizon Tire fornece produtos para pneus nos Estados Unidos há mais de 10 anos. <br> Top O.E.M., Distribuidores e varejistas reconhecem o fornecedor escolhido tanto pelo serviço quanto pela confiabilidade e qualidade do produto. <br> A Horizon tem um seguro de responsabilidade do produto emitido pela companhia de seguros dos EUA em todos os produtos da nossa oferta.");
                    $("#contentModalButton").html("<a href='http://www.horizontire.com/catalog/2018_Rydanz_Catalog.pdf' target='_blank' class='btn btn-link'>Saiba mais...</a>");
                    break;
              }

            }
        </script>

        <script type="text/javascript" src="{{ asset('asset/js/floating-wpp.min.js')}}"></script>
        <script type="text/javascript">
          $(function () {
              $('.floating-wpp').floatingWhatsApp({
                  phone: '558888716030',
                  popupMessage: 'Olá, estamos aguardando o seu contato!',
                  showPopup: true,
                  position: 'right', // left or right
                  autoOpen: false, // true or false
                  //autoOpenTimer: 4000,
                  message: '',
                  //headerColor: 'orange', // enable to change msg box color
                  headerTitle: 'Rastro Pneus Whatsapp',
              });
          });
        </script>
        <div class="floating-wpp"></div>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-80183246-28"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-80183246-28');
        </script>


    </body>
    </html>
