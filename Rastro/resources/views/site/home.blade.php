@extends('layouts.landing')
@section('content')
<!-- Header -->
<header>
  <div class="row">
    <div id="carouselHome" class="carousel slide" data-ride="carousel">

      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carouselHome" data-slide-to="0" class="active"></li>
        <li data-target="#carouselHome" data-slide-to="1"></li>
        <li data-target="#carouselHome" data-slide-to="2"></li>
        <li data-target="#carouselHome" data-slide-to="3"></li>
        <li data-target="#carouselHome" data-slide-to="4"></li>
        <li data-target="#carouselHome" data-slide-to="5"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" >
        <div class="item active">
          <img src="{{ asset('asset/img/banner-abril.png')}}" style="height:auto; margin:0 auto;">
        </div>
        <div class="item">
          <img src="{{ asset('asset/img/Rastrosite2.png')}}" style="height:auto; margin:0 auto;">
        </div>
        <!--
        <div class="item">
        <img src="img/banner1.png" style="height:auto; margin:0 auto;">
      </div>
    -->
    <div class="item">
      <img src="{{ asset('asset/img/banner3.png')}}" style="height:auto; margin:0 auto;">
    </div>
    <div class="item">
      <img src="{{ asset('asset/img/banner-junho.png')}}" style="height:auto; margin:0 auto;">
    </div>
    <div class="item">
      <img src="{{ asset('asset/img/west.png')}}" style="height:auto; margin:0 auto;">
    </div>
    <div class="item">
      <img src="{{ asset('asset/img/ovation.png')}}" style="height:auto; margin:0 auto;">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#carouselHome" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="right carousel-control" href="#carouselHome" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Próximo</span>
  </a>
</div>
</div>
</header>

<section class="services-home bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 title text-center">
        <h2>Serviços</h2>
      </div>
    </div>
    <div class="row services-line">
      <div class="col-lg-3 col-md-6">
        <img src="{{ asset('asset/img/alinhamento-index.jpg')}}">
        <h3>Alinhamento Computadorizado</h3>
        <p>Ajuste de posição das rodas na suspensão</p>
      </div>

      <div class="col-lg-3 col-md-6">
        <img src="{{ asset('asset/img/balanceamento-index.jpg')}}">
        <h3>Balanceamento</h3>
        <p>Restauração do equilíbrio entre as rodas e os pneus</p>
      </div>

      <div class="col-lg-3 col-md-6">
        <img src="{{ asset('asset/img/cambagem-index.jpg')}}">
        <h3>Cambagem</h3>
        <p>Ajuste da inclinação das rodas de um veículo em relação a um plano vertical</p>
      </div>

      <div class="col-lg-3 col-md-6">
        <img src="{{ asset('asset/img/recuperacao-index.jpg')}}">
        <h3>Recuperação de Rodas</h3>
        <p>Reparo e pintura de rodas</p>
      </div>

    </div>
  </div>
</section>

<br>
<br>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="title"><i class="fa fa-map fa-1x" style="color:#B80408;"></i> VISITE-NOS! <br><small style="font-size:12px;">Conheça nosso ambiente</small></h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.2498913894597!2d-39.32498178549154!3d-7.212308994793246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7a1820860986633%3A0xab2f2fa2372b0dc2!2sRastro+Pneus!5e0!3m2!1spt-BR!2sbr!4v1527092723130" width="570" height="350" frameborder="0" style="border:0; max-width:100%;"></iframe>
      </div>
      <div class="col-md-6">
        <h2 class="title"><i class="fa fa-phone fa-1x" style="color:#B80408;"></i> Ligamos para você! <br><small style="font-size:12px;">Preencha o formulário e envie que ligamos para você</small></h2>
        <div class="row">
          <div class="col-md-12">
            <form action="{{ url('Ligar/Enviar')}}" id="ligar-form" method="post" class="form" role="form">
              @csrf
              <div class="row">
                <div class="col-xs-12 col-md-12 form-group">
                  <input class="form-control" id="nome" name="nome" placeholder="Seu nome" type="text" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-12 form-group">
                  <input class="form-control" id="fone" name="fone" placeholder="Telefone" type="text" data-mask="(00) 0 0000-0000" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-12 form-group">
                  <input class="form-control" id="interesse" name="interesse" placeholder="Assunto" type="text" required="">
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-xs-12 form-group">
                  <button class="btn btn-danger btn-lg pull-right"  type="submit">Enviar</button>
                </div>
              </div></form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="payment bg-2">
    <div class="container">
      <div class="row text-center">
        <div class="col-lg-12">
          <img src="{{ asset('asset/img/pagamento.png')}}" style="width:100%;">
        </div>
      </div>
    </div>
  </section>


  <section class="brands">
    <div class="container">
      <div class="row">
        <div class="customer-logos">
          <div role="button" class="slide" id="div01" onClick="showDescription(1)"><img src="{{ asset('asset/img/parceiros/01.png')}}" data-toggle="modal" data-target="#myModal"></div>

          <div role="button" class="slide" id="div02" onClick="showDescription(2)"><img src="{{ asset('asset/img/parceiros/02.png')}}" data-toggle="modal" data-target="#myModal"></div>
          <div role="button" class="slide" id="div03" onClick="showDescription(3)"><img src="{{ asset('asset/img/parceiros/03.png')}}" data-toggle="modal" data-target="#myModal"></div>
          <div role="button" class="slide" id="div04" onClick="showDescription(4)"><img src="{{ asset('asset/img/parceiros/04.png')}}" data-toggle="modal" data-target="#myModal"></div>
          <div role="button" class="slide" id="div05" onClick="showDescription(5)"><img src="{{ asset('asset/img/parceiros/05.png')}}" data-toggle="modal" data-target="#myModal"></div>
          <div role="button" class="slide" id="div06" onClick="showDescription(6)"><img src="{{ asset('asset/img/parceiros/06.png')}}" data-toggle="modal" data-target="#myModal"></div>
          <div role="button" class="slide" id="div07" onClick="showDescription(7)"><img src="{{ asset('asset/img/parceiros/07.png')}}" data-toggle="modal" data-target="#myModal"></div>
          <div role="button" class="slide" id="div08" onClick="showDescription(8)"><img src="{{ asset('asset/img/parceiros/08.png')}}" data-toggle="modal" data-target="#myModal"></div>

        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <img src="{{ asset('asset/img/pneu.png')}}" class="img-responsive">
        </div>
      </div>
    </div>
  </section>

  <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="titleModal">Modal title</h4>
        </div>
        <div class="modal-body">
          <p id="contentModal"></p>
          <div class="modal-body">
            <div class='row'>
              <div class='col-md-3 col-md-offset-3'></div>
              <div class='col-md-2 col-md-offset-3' id="contentModalButton"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
@endsection
