@extends('layouts.landing')
@section('content')
<section class="head" style="background-image: url('{{ asset('asset/img/servicos.jpg')}}'); background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2>Nossos Serviços</h2>
			</div>
		</div>
	</div>
</section>


<section class="servicos">
			<div class="container">
				<div class="row itens-servicos">

					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/cambagem.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Cambagem</h3>

							<br>

							<p>
								A cambagem tem a tarefa de distribuir o peso do carro sobre a banda de rodagem, assim evitando que a vida útil do pneu diminua, bem como alterações indesejáveis na direção.
							</p>
							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/rodas-rec.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Recuperação de Rodas</h3>

							<br>

							<p>
								Conserto e pintura de rodas raladas, riscadas ou amassadas. Após a recuperação as rodas elas vão ficar como novas.
							</p>

							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/s6.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Alinhamento Computadorizado</h3>

							<br>

							<p>
								 Processo que regula os ângulos de direção e suspensão do carro, projetado para minimizar o desgaste e também maximizar o conforto do motorista.
							</p>

							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
				</div>


				<!--linha 2-->
				<div class="row itens-servicos">

					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/s4.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Balanceamento</h3>

							<br>

							<p>
									O Balanceamento de rodas consiste em equilibrar (compensar) o excesso ou má distribuição de massas no conjunto pneu e roda.
							</p>
							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/s5.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Troca de Óleo</h3>

							<br>
							<p>A troca do filtro e do óleo do carro é um procedimento importante para o funcionamento ideal do veículo.</p>

							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/montagem.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Montagem</h3>

							<br>
							<p>
								Montagens e a desmontagens de pneus efetuadas por profissionais treinados e qualificados, utilizando ferramentas e procedimentos adequados.
							</p>

							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
				</div>
				<!--./linha 2-->

				<!--linha 3-->
				<div class="row itens-servicos">

					<div class="col-md-4" style="padding: 0">
						<img src="{{ asset('asset/img/s1.jpg')}}" width="100%">

						<div class="campo_informacao text-center">
							<h3>Revisões Básicas</h3>

							<br>

							<p>
									Troca de pastilhas, disco de freio e troca de filtros em geral.
							</p>
							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
					<!-- <div class="col-md-4" style="padding: 0">
						<img src="img/s5.jpg" width="100%">

						<div class="campo_informacao text-center">
							<h3>Troca de Óleo</h3>

							<br>
							<p>A troca do filtro e do óleo do carro é um procedimento importante para o funcionamento ideal do veículo.</p>

							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div>
					<div class="col-md-4" style="padding: 0">
						<img src="img/s6.jpg" width="100%">

						<div class="campo_informacao text-center">
							<h3>Montagem</h3>

							<br>
							<p>
								Montagens e a desmontagens de pneus efetuadas por profissionais treinados e qualificados, utilizando ferramentas e procedimentos adequados.
							</p>

							<p>
								<a href="contato.php" class="btn btn-danger pull-right">+ Informações</a>
							</p>

						</div>
					</div> -->
				</div>
				<!--./linha 3-->
			</div>
		</section>

@endsection
