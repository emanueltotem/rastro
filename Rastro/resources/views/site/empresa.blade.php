@extends('layouts.landing')
@section('content')
<section class="head" style="background-image: url('{{ asset('asset/img/rastro.jpg')}}'); background-size: cover; background-position: center top;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2>A Empresa</h2>
			</div>
		</div>
	</div>
</section>

<section class="default">
	<div class="container">
		<div class="row">

			<div class="col-lg-8">
				<h1>Quem Somos</h1>
				<p>Em 2005, os sócios Leonardo Tavares e Daniel Machado, resolveram começar a vender pneus usados, os chamados "meia vida", sem um ponto fixo comercial. Com o passar do tempo, essa atividade se expandiu para uma borracharia e em 2006, os sócios conseguiram alugar um espaço no centro de Juazeiro do Norte, que foi crescendo continuamente e hoje em dia é a Rastro Pneus.</p>

				<p>Localizada na Avenida Pe. Cícero, a Rastro conta os seguintes serviços:</p>
					<ul>
						<li>Venda de pneus novos</li>
						<li>Alinhamento</li>
						<li>Balanceamento</li>
						<li>Cambagem</li>
						<li>Troca de Óleo</li>
						<li>Troca de Filtros em geral</li>
						<li>Revisões Básicas</li>
					</ul>
            <p>Com 12 anos de mercado, a Rastro Pneus é uma das maiores revendedoras multimarcas de pneus do Ceará, distribuindo marcas como Dunlop, Yokohama, Ovation, Fateo, Pirelli, Aeulos e GT Radial.</p>
				<br>

				<h3>Missão</h3>
				<p>Cuidar da sua segurança e de quem você mais ama.</p>

				<br>

				<h3>Visão</h3>
				<p>Ser um dos maiores revendedores e distribuidores multimarca do Estado do Ceará.</p>

				<br>

				<h3>Valores</h3>
				<!--<p>Nossos valores são fundamentados na ética corporativa e interpessoal, transparência, respeito ao próximo, meio-ambiente e à nossa missão e visão.</p>-->
				<ul>
						<li>Ética corporativa e interpessoal</li>
						<li>Transparência</li>
						<li>Respeito ao próximo</li>
						<li>Respeito ao meio-ambiente</li>
						<li>Respeito à nossa missão e visão</li>
					</ul>
			</div>

			<div class="col-lg-4 sidebar">
				<img src="{{ asset('asset/img/icon-clock.png')}}">
				<div class="title-sidebar">
					Atendimento<br>ao Cliente
				</div>
				<div class="fone-sidebar">
					<span>88</span> 3587.4195
				</div>
			</div>


		</div>
	</div>
</section>
@endsection
