<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>
    Rastro - Dashboard
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper">
    <div class="sidebar" data-color="danger" data-background-color="white" style="display:none;">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo">
      <a href="{{ url('Adm')}}" class="simple-text logo-normal">
        Creative Tim
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item active  ">
          <a class="nav-link" href="{{ url('Adm')}}">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
          </a>
        </li>
        <!-- your sidebar here -->
      </ul>
    </div>
  </div>
  <div class="main-panel" style="width:100%;">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <a class="navbar-brand" href="{{ url('Adm')}}">Painel de Controle</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon icon-bar"></span>
          <span class="navbar-toggler-icon icon-bar"></span>
          <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('Adm/Email')}}">Para E-mail Marketing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('Adm/Ligar')}}">Para Ligar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('Adm/Contato')}}">Para Contato</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">person</i>
                <p class="d-lg-none d-md-block">
                  Ações
                </p>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#" style="display:none;">Perfil</a>
                <a class="dropdown-item" href="{{ url('Sair')}}">Sair</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
      <div class="container-fluid">
        @yield('content')
      </div>
    </div>
    <footer class="footer">
      <div class="container-fluid">
        <nav class="float-left">
          <ul>
            <li>
              <a href="https://www.creative-tim.com">
                Aller Soluções
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright float-right">
          Admin &copy;
          <script>
          document.write(new Date().getFullYear())
          </script>
        </div>
      </div>
    </footer>
  </div>
</div>



<!--   Core JS Files   -->
<script src="{{ asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="{{ asset('assets/js/plugins/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript')}}"></script>
<script>

  function showObject(nome, email, fone, interesse, descricao, dataregistro){
    $("#bNome").html(nome);
    $("#bEmail").html(email);
    $("#bFone").html(fone);
    $("#bInteresse").html(interesse);
    $("#bDescricao").html(descricao);
    $("#bData").html(dataregistro);
  }

  function filter(nome){
    var objs = $(".objects");
    if (nome != "") {
        for (var i = 0; i < objs.length; i++) {
            if (objs[i].getAttribute('nome').toLowerCase().search(nome.toLowerCase())) {
                objs[i].style.display = "none";
            }
        }
    }else{
      for (var i = 0; i < objs.length; i++) {
          if (i < 15) {
              objs[i].style = "";
          }
      }
    }
  }

  function confirmCall(id){
    $.post("{{ url('Adm/Ligar/Confirmar')}}", { 'id':id, '_token': $('meta[name=csrf-token]').attr('content') }, function(response){
        if(response != null){
          alert(response);
          $("#"+id+"_element_column").html('Ligado!');
        }
    });
  }
</script>
</body>

</html>
