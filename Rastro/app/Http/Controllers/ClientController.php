<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $status = "";
        $request['tipo'] = 'contato';
        $client = Client::all()->where('email',$request['email'])->first();
        if(!$client){
          Client::create($request->all());
          $status = "Registro realizado com sucesso!";
        }

        return $status;
    }

    public function email(Request $request)
    {
        //
        $status = "";
        $request['tipo'] = 'email';
        $client = Client::all()->where('email',$request['email'])->first();
        if(!$client){
          Client::create($request->all());
          $status = "Registro realizado com sucesso!";
        }

        return $status;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $status = "";
        $request['status_ligacao'] = false;
        $request['tipo'] = 'ligar';
        $client = Client::all()->where('fone',$request['fone'])->first();
        if(!$client){
          Client::create($request->all());
          $status = "Registro realizado com sucesso!";
        }

        return $status;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
