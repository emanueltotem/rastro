<?php

namespace App\Http\Controllers\Adm;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clients = Client::all()->where('tipo','contato');
        $today = $clients->where('created_at','>=',date('Y-m-d'));
        $clients = $clients->sortByDesc('created_at');
        return view('adm.contato.index')->with(['clients'=>$clients,'today'=>$today]);
    }

    /**
    * Export all clients where created_at = today and your type is ligar in pdf
    *
    * @param  \App\Client  $client
    * @return \Illuminate\Http\Response
    */
    public function exportToday()
    {
      //
      $clients = Client::all()->where('tipo','contato');
      $clients = $clients->where('created_at','>=',date('Y-m-d'));
      $pdf = PDF::loadView('adm.contato.pdf', compact('clients'));
      return $pdf->download(date('d-m-Y').'_clientes-para-ligar.pdf');
    }


    /**
    * Export all clients in pdf
    *
    * @param  \App\Client  $client
    * @return \Illuminate\Http\Response
    */
    public function exportAll()
    {
      //
      $clients = Client::all()->where('tipo','contato');
      $pdf = PDF::loadView('adm.contato.pdf', compact('clients'));
      return $pdf->download(date('d-m-Y').'_todos-clientes-para-ligar.pdf');
    }
}
