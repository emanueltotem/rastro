<?php

namespace App\Http\Controllers\Adm;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clients = Client::all()->where('tipo','email');
        $today = $clients->where('created_at','>=',date('Y-m-d'));
        $clients = $clients->sortByDesc('created_at');
        return view('adm.email.index')->with(['clients'=>$clients,'today'=>$today]);
    }

    /**
    * Export all clients where created_at = today and your type is ligar in pdf
    *
    * @param  \App\Client  $client
    * @return \Illuminate\Http\Response
    */
    public function exportToday()
    {
      //
      $clients = Client::all()->where('tipo','email');
      $clients = $clients->where('created_at','>=',date('Y-m-d'));
      $pdf = PDF::loadView('adm.email.pdf', compact('clients'));
      return $pdf->download(date('d-m-Y').'_clientes-para-ligar.pdf');
    }


    /**
    * Export all clients in pdf
    *
    * @param  \App\Client  $client
    * @return \Illuminate\Http\Response
    */
    public function exportAll()
    {
      //
      $clients = Client::all()->where('tipo','email');
      $pdf = PDF::loadView('adm.email.pdf', compact('clients'));
      return $pdf->download(date('d-m-Y').'_todos-email-para-ligar.pdf');
    }
}
