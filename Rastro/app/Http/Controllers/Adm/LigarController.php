<?php

namespace App\Http\Controllers\Adm;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class LigarController extends Controller
{

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
    $clients = Client::all()->where('tipo','ligar');
    $today = $clients->where('created_at','>=',date('Y-m-d'));
    $clients = $clients->sortByDesc('created_at');
    return view('adm.ligar.index')->with(['clients'=>$clients,'today'=>$today]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request)
  {
    //
    $client = Client::find($request['id']);
    $status = "Erro ao realizar confirmação! Tente novamente mais tarde!";

    if($client){
      $client->status_ligacao = true;
      $client->save();
      //
      $status = "Ligação confirmada com sucesso!";
    }

    return $status;
  }

  /**
  * Export all clients where created_at = today and your type is ligar in pdf
  *
  * @param  \App\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function exportToday()
  {
    //
    $clients = Client::all()->where('tipo','ligar');
    $clients = $clients->where('created_at','>=',date('Y-m-d'));
    $pdf = PDF::loadView('adm.ligar.pdf', compact('clients'));
    return $pdf->download(date('d-m-Y').'_clientes-para-ligar.pdf');
  }


  /**
  * Export all clients in pdf
  *
  * @param  \App\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function exportAll()
  {
    //
    $clients = Client::all()->where('tipo','ligar');
    $pdf = PDF::loadView('adm.ligar.pdf', compact('clients'));
    return $pdf->download(date('d-m-Y').'_todos-clientes-para-ligar.pdf');
  }

}
