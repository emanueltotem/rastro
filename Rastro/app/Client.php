<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = [
        'tipo', 'nome', 'email', 'fone', 'interesse', 'mensagem_contato', 'status_ligacao'
    ];
}
