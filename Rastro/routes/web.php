<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
});

Route::get('register', function () {
    return redirect('login');
});

Route::get('Empresa', function () {
    return view('site.empresa');
});

Route::get('Servicos', function () {
    return view('site.servicos');
});

Route::get('Contato', function () {
    return view('site.contato');
});

Route::post('Contato/Enviar', 'ClientController@create');

Route::post('Ligar/Enviar', 'ClientController@store');

Route::post('Email/Enviar', 'ClientController@email');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth','prefix'=>'Adm'],function(){

  Route::get('','Adm\EmailController@index');

  Route::get('Contato','Adm\ContatoController@index');

  Route::get('Contato/Exportar/Hoje','Adm\ContatoController@exportToday');

  Route::get('Contato/Exportar','Adm\ContatoController@exportAll');

  Route::get('Ligar','Adm\LigarController@index');

  Route::post('Ligar/Confirmar','Adm\LigarController@update');

  Route::get('Ligar/Exportar/Hoje','Adm\LigarController@exportToday');

  Route::get('Ligar/Exportar','Adm\LigarController@exportAll');

  Route::get('Email','Adm\EmailController@index');

  Route::get('Email/Exportar/Hoje','Adm\EmailController@exportToday');

  Route::get('Email/Exportar','Adm\EmailController@exportAll');

});

Route::get('Sair', function(){
  Auth::logout();
  return redirect('/');
});
