@extends('layouts.adm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-8">
        <h3 class="pull-left"></h3>
      </div>
      <div class="col-md-4">
        <button class="btn btn-outline-primary pull-right" onclick="showRow(this)" value="0"><i class="fa fa-plus"></i> Add</button>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="container-fluid">
              <br>
              <div class="row" id="rowList">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table datatable">
                      <thead>
                        <th>Nome</th>
                        <th>Cargo</th>
                        <th class="text-center">Ações</th>
                      </thead>
                      <tbody>
                        @foreach($colaboradors as $colaborador)
                        <td>
                        </td>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="container-fluid" id="rowAdd" style="display:none;">

                <div class="row">

                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#pessoais" aria-controls="pessoais" role="tab" data-toggle="tab">Dados Pessoais</a></li>
                    <li role="presentation"><a href="#cargo" aria-controls="cargo" role="tab" data-toggle="tab">Vínculo/Cargo</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Dados Complementares</a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Documentos</a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Contatos</a></li>
                  </ul>

                  <div class="row">
                    <div class="col-md-12">
                      <form method="post" action="">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="pessoais">
                            <br>
                            <div class="form-group row">
                              <div class="col-md-4">
                                <label class="control-label">Matrícula *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-8">
                                <label class="control-label">Nome completo *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-3">
                                <label class="control-label">RG *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-2">
                                <label class="control-label">Data de Registro (RG) *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-3">
                                <label class="control-label">CPF *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-2">
                                <label class="control-label">Data de Registro (CPF) *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-2">
                                <label class="control-label">Data de Nascimento *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-4">
                                <label class="control-label">Foto *</label>
                                <input type="file" class="form-control" name="">
                              </div>
                              <div class="col-md-3">
                                <label class="control-label">Estado Social *</label>
                                <select class="form-control">
                                  <option>Selecione o estado social</option>
                                  <option></option>
                                </select>
                              </div>
                              <div class="col-md-2">
                                <label class="control-label">Alguma Deficiência?</label>
                                <select class="form-control">
                                  <option value="0">Não</option>
                                  <option value="1">Sim</option>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <label class="control-label">Tipo da Deficiência</label>
                                <select class="form-control" disabled>
                                  <option value="">Selecione o tipo da deficiência</option>
                                  <option value="1">Motora</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-">
                              </div>
                            </div>
                          </div>
                          <div role="tabpanel" class="tab-pane" id="cargo">
                            <br>
                            <div class="form-group row">
                              <div class="col-md-4">
                                <label class="control-label">Cargo * <button class="btn btn-sm pull-right"><i class="fa fa-plus"></i></button></label>
                                <select class="form-control">
                                  <option value="">Selecione o cargo do colaborador</option>
                                  @foreach($cargos as $cargo)
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-md-4">
                                <label class="control-label">Setor * <button class="btn btn-sm pull-right"><i class="fa fa-plus"></i></button></label>
                                <select class="form-control">
                                  <option value="">Selecione o setor do colaborador</option>
                                </select>
                              </div>
                              <div class="col-md-2">
                                <label class="control-label">Carga Horária *</label>
                                <input type="tel" class="form-control" name="">
                              </div>
                              <div class="col-md-2">
                                <label class="control-label">Turno *</label>
                                <select class="form-control" name="">
                                  <option value="">Selecione o turno do colaborador</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-4">
                                <label class="control-label">RG *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-4">
                                <label class="control-label">CPF *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                              <div class="col-md-4">
                                <label class="control-label">Data de Nascimento *</label>
                                <input type="text" class="form-control" name="" required>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" onclick=""><i class="fa fa-check"></i> Cadastrar</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal add -->
@endsection
