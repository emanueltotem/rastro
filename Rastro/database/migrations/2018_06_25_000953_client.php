<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Client extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status_ligacao')->default('true');
            $table->enum('tipo',array('email','contato','ligar'))->default('email');
            $table->string('nome')->nullable();
            $table->string('email')->nullable();
            $table->integer('fone')->nullable();
            $table->string('interesse')->nullable();
            $table->string('mensagem_contato')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
